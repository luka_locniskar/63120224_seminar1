﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="_63120224_Seminar1.Order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 301px">
    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderHeader.SalesOrderNumber, SalesLT.SalesOrderHeader.OrderDate, SalesLT.SalesOrderHeader.DueDate, SalesLT.Customer.CompanyName, SalesLT.Address.AddressLine1 + ', ' + SalesLT.Address.City + ', ' + SalesLT.Address.PostalCode + ', ' + SalesLT.Address.CountryRegion AS Expr2, SalesLT.Customer.EmailAddress, SalesLT.Customer.Phone, SalesLT.SalesOrderHeader.ShipDate, SalesLT.SalesOrderHeader.ShipMethod, SalesLT.Address.AddressLine1 + ', ' + SalesLT.Address.City + ', ' + SalesLT.Address.PostalCode + ', ' + SalesLT.Address.CountryRegion AS EXPR1, SalesLT.SalesOrderHeader.SubTotal, SalesLT.SalesOrderHeader.TaxAmt, SalesLT.SalesOrderHeader.Freight, SalesLT.SalesOrderHeader.TotalDue, SalesLT.SalesOrderHeader.SalesOrderID FROM SalesLT.SalesOrderHeader INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID WHERE (SalesLT.SalesOrderHeader.SalesOrderID = @FilterSOID)">
            <SelectParameters>
                <asp:QueryStringParameter Name="FilterSOID" QueryStringField="id" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.Product.ProductNumber, SalesLT.Product.Name, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal, SalesLT.SalesOrderDetail.SalesOrderID FROM SalesLT.Product INNER JOIN SalesLT.SalesOrderDetail ON SalesLT.Product.ProductID = SalesLT.SalesOrderDetail.ProductID WHERE (SalesLT.SalesOrderDetail.SalesOrderID = @Filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DetailsView1" Name="Filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Label ID="L_Orders" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Naročilo"></asp:Label>
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
        <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="125px" AutoGenerateRows="False" DataKeyNames="SalesOrderID" DataSourceID="SqlDataSource1">
            <Fields>
                <asp:BoundField DataField="SalesOrderNumber" HeaderText="Številka naročila" ReadOnly="True" SortExpression="SalesOrderNumber" />
                <asp:BoundField DataField="OrderDate" HeaderText="Datum naročila" SortExpression="OrderDate" DataFormatString="{0:d}" />
                <asp:BoundField DataField="DueDate" HeaderText="Datum valute" SortExpression="DueDate" DataFormatString="{0:d}" />
                <asp:BoundField DataField="CompanyName" HeaderText="Naziv" SortExpression="CompanyName" />
                <asp:BoundField DataField="Expr2" HeaderText="Naslov" ReadOnly="True" SortExpression="Expr2" />
                <asp:BoundField DataField="EmailAddress" HeaderText="Elektronska pošta" SortExpression="EmailAddress" />
                <asp:BoundField DataField="Phone" HeaderText="Telefon" SortExpression="Phone" />
                <asp:BoundField DataField="ShipDate" HeaderText="Datum dostave" SortExpression="ShipDate" DataFormatString="{0:d}" />
                <asp:BoundField DataField="ShipMethod" HeaderText="Način dostave" SortExpression="ShipMethod" />
                <asp:BoundField DataField="EXPR1" HeaderText="Naslov za dostavo" ReadOnly="True" SortExpression="EXPR1" />
                <asp:BoundField DataField="SubTotal" HeaderText="Skupaj" SortExpression="SubTotal" DataFormatString="{0:c}" />
                <asp:BoundField DataField="TaxAmt" HeaderText="Davek" SortExpression="TaxAmt" DataFormatString="{0:c}" />
                <asp:BoundField DataField="Freight" HeaderText="Dostava" SortExpression="Freight" DataFormatString="{0:c}" />
                <asp:BoundField DataField="TotalDue" HeaderText="Za plačilo" ReadOnly="True" SortExpression="TotalDue" DataFormatString="{0:c}" >
                <ControlStyle Font-Bold="True" />
                <HeaderStyle Font-Bold="True" />
                <ItemStyle Font-Bold="True" />
                </asp:BoundField>
                <asp:BoundField DataField="SalesOrderID" HeaderText="SalesOrderID" InsertVisible="False" ReadOnly="True" SortExpression="SalesOrderID" Visible="False" />
            </Fields>
        </asp:DetailsView>
        <hr />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
            <Columns>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Izdelek" SortExpression="Name" />
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" />
                <asp:BoundField DataField="UnitPrice" HeaderText="Cena na enoto" SortExpression="UnitPrice" DataFormatString="{0:c}" />
                <asp:BoundField DataField="UnitPriceDiscount" HeaderText="Popust" SortExpression="UnitPriceDiscount" DataFormatString="{0:p}" />
                <asp:BoundField DataField="LineTotal" HeaderText="Znesek" ReadOnly="True" SortExpression="LineTotal" DataFormatString="{0:c}" />
                <asp:BoundField DataField="SalesOrderID" HeaderText="SalesOrderID" SortExpression="SalesOrderID" Visible="False" />
            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
