﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace _63120224_Seminar1
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

              String lukabase = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
              SqlConnection myConnection = new SqlConnection(lukabase);
              myConnection.Open();
              string sql = "Select SalesLT.Product.ThumbNailPhoto from SalesLT.Product where SalesLT.Product.ProductID=@FilterLuka";
              SqlCommand cmd = new SqlCommand(sql, myConnection);
              cmd.Parameters.Add("@FilterLuka", SqlDbType.Int).Value = context.Request.QueryString["id"];
              cmd.Prepare();
              SqlDataReader dr = cmd.ExecuteReader();
              dr.Read();
              context.Response.BinaryWrite((byte[])dr["ThumbNailPhoto"]);
              dr.Close();
              myConnection.Close();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}