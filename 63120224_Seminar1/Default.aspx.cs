﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63120224_Seminar1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (GV_Products.SelectedIndex > -1)
            {
                DV_Product.Visible = true;
                GV_Orders.Visible = true;   
            }

            L_Product.Visible = false;
            TB_FilterOrders.Visible = false;
            L_Orders.Visible = false;
        }

        protected void GV_Orders_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Response.Redirect("Order.aspx?id=" + GV_Orders.SelectedDataKey.Value.ToString());
            string url = "Order.aspx?id=" + GV_Orders.SelectedValue.ToString();
            string details = "(window.open('" + url + "','Order', 'width = 500, resizable = 1," +
                " scrollbars = 1, status=1')).focus()";
            ClientScript.RegisterStartupScript(this.GetType(), "details", details, true);
        }


        protected void GV_Orders_RowCreated(object sender, GridViewRowEventArgs e)
        {
            TB_FilterOrders.Visible = true;
            L_Orders.Visible = true;
         
        }

        protected void DV_Product_ItemCreated(object sender, EventArgs e)
        {
            if (DV_Product.Rows.Count>0)
            {
                L_Product.Visible = true;
            }
        }

        protected void DV_Product_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            L_Product.Visible = true;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDL_CSS.SelectedIndex.Equals(0))
            {
                stylesheet.Href = "CSSDefault.css";
            }
            else if (DDL_CSS.SelectedIndex.Equals(1))
            {
                stylesheet.Href = "CSS1.css";
            }
            else if (DDL_CSS.SelectedIndex.Equals(1))
            {
                stylesheet.Href = "CSS2.css";
            }
        }
    }
}