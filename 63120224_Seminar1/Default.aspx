﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63120224_Seminar1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="CSSDefault.css" id="stylesheet"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 1620px;
        }
    </style>
</head>
<body style="height: 1016px">
    <form id="form1" runat="server">
    <div>
    
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID, ParentProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModel.Name, SalesLT.ProductDescription.Description, SalesLT.ProductModelProductDescription.Culture, SalesLT.Product.ProductCategoryID, SalesLT.ProductModel.ProductModelID FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.Product.ProductCategoryID = @FilterC) AND (SalesLT.ProductModelProductDescription.Culture = N'en')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="FilterC" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ParentProductCategoryID, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @Filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" DefaultValue="" Name="Filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductCategoryID, ProductModelID, ProductNumber, Name, Size, Color, ListPrice, ProductID, ThumbNailPhoto, ThumbnailPhotoFileName FROM SalesLT.Product WHERE (ProductCategoryID = @FilterCat) AND (ProductModelID = @FilterMod)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="FilterCat" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="GV_Models" Name="FilterMod" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, Weight, StandardCost, ListPrice, ProductID FROM SalesLT.Product WHERE (ProductID = @FilterPID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="FilterPID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.SalesOrderHeader.OrderDate, SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ', ' + SalesLT.Address.CountryRegion + ')' AS Expr1, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal, SalesLT.SalesOrderDetail.ProductID FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID WHERE (SalesLT.SalesOrderDetail.ProductID = @filterPID) AND (SalesLT.Customer.CompanyName LIKE N'%' + @filterC + N'%')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filterPID" PropertyName="SelectedValue" DefaultValue="" />
                <asp:ControlParameter ControlID="TB_FilterOrders" DefaultValue="%" Name="filterC" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DropDownList ID="DDL_CSS" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem>Default</asp:ListItem>
            <asp:ListItem>CSS1</asp:ListItem>
            <asp:ListItem>CSS2</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="L_Categories" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Oddelki in skupine izdelkov"></asp:Label>
    
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="F1, F2, F3, F4"></asp:Label>
    
    </div>
        <asp:DropDownList ID="DDL_Categories" runat="server" AutoPostBack="True" BackColor="#33CCFF" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="ProductCategoryID" ForeColor="Black" OnSelectedIndexChanged="Page_Load">
        </asp:DropDownList>
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" BackColor="White" DataSourceID="SqlDataSource1" Height="137px" Width="219px" DataKeyNames="ProductCategoryID" SelectedIndex="0" OnSelectedIndexChanged="Page_Load">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:CommandField ButtonType="Button" SelectText="&gt;" ShowSelectButton="True">
                <ItemStyle BorderStyle="None" />
                </asp:CommandField>
                <asp:BoundField DataField="ParentProductCategoryID" HeaderText="ParentProductCategoryID" SortExpression="ParentProductCategoryID" Visible="False" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" InsertVisible="False" ReadOnly="True" SortExpression="ProductCategoryID" Visible="False" />
            </Columns>
            <EmptyDataTemplate>
                <asp:Button ID="Button1" runat="server" Text="Button" />
            </EmptyDataTemplate>
            <SelectedRowStyle BackColor="#33CCFF" />
        </asp:GridView>
        <hr />
        <br />
        <asp:Label ID="L_Categories0" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Modeli"></asp:Label>
        <asp:GridView ID="GV_Models" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" Width="565px" DataKeyNames="ProductModelID" SelectedIndex="0" OnSelectedIndexChanged="Page_Load">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:CommandField SelectText="Prikaži izdelke" ShowSelectButton="True" />
                <asp:BoundField DataField="Culture" HeaderText="Culture" SortExpression="Culture" Visible="False" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" SortExpression="ProductCategoryID" Visible="False" />
            </Columns>
            <SelectedRowStyle BackColor="#33CCFF" CssClass="SelectedRow" />
        </asp:GridView>
        <hr />
        <asp:Label ID="L_Categories1" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Izdelki"></asp:Label>
    
        <asp:GridView ID="GV_Products" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource4" OnSelectedIndexChanged="Page_Load">
            <Columns>
                <asp:TemplateField HeaderText="Slika"><ItemTemplate><asp:Image ID="Image1" runat="server" ImageUrl='<%#    "Handler1.ashx?id=" + Eval("ProductID")  %>' /> </ItemTemplate> </asp:TemplateField>
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" SortExpression="ProductCategoryID" Visible="False" />
                <asp:BoundField DataField="ProductModelID" HeaderText="ProductModelID" SortExpression="ProductModelID" Visible="False" />
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Cena" SortExpression="ListPrice" />
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <SelectedRowStyle BackColor="#33CCFF" />
        </asp:GridView>
        <hr />
        <p>
        <asp:Label ID="L_Product" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Podatki o izdelku" Visible="False"></asp:Label>
    
        </p>
        <asp:DetailsView ID="DV_Product" runat="server" AutoGenerateRows="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource5" Height="50px" Width="366px" Visible="False" OnItemCreated="DV_Product_ItemCreated" OnItemUpdating="DV_Product_ItemUpdating">
            <Fields>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Weight" HeaderText="Teža" SortExpression="Weight" />
                <asp:BoundField DataField="StandardCost" DataFormatString="{0:c}" HeaderText="Nabavna cena" SortExpression="StandardCost" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Prodajna cena" SortExpression="ListPrice" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" />
            </Fields>
        </asp:DetailsView>
        <br />
        <asp:Label ID="L_Orders" runat="server" Font-Size="Large" ForeColor="#0066FF" Text="Seznam naročil" Visible="False"></asp:Label>
    
        <br />
        <asp:TextBox ID="TB_FilterOrders" runat="server" AutoPostBack="True" Visible="False"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
        <asp:GridView ID="GV_Orders" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource6" Visible="False" OnSelectedIndexChanged="GV_Orders_SelectedIndexChanged" DataKeyNames="SalesOrderID" OnRowCreated="GV_Orders_RowCreated">
            <Columns>
                <asp:BoundField DataField="SalesOrderID" SortExpression="SalesOrderID" />
                <asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="Datum" SortExpression="OrderDate" />
                <asp:BoundField DataField="Expr1" HeaderText="Kupec" ReadOnly="True" SortExpression="Expr1" />
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" />
                <asp:BoundField DataField="UnitPrice" DataFormatString="{0:c}" HeaderText="Cena enote" SortExpression="UnitPrice" />
                <asp:BoundField DataField="UnitPriceDiscount" DataFormatString="{0:p}" HeaderText="Popust" SortExpression="UnitPriceDiscount" />
                <asp:BoundField DataField="LineTotal" DataFormatString="{0:c}" HeaderText="Cena" ReadOnly="True" SortExpression="LineTotal" />
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
